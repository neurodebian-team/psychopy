psychopy (2024.2.4+dfsg-1) UNRELEASED; urgency=medium

  [ Étienne Mollier ]
  * New upstream version
  * d/copyright: exclude MentalRotation.psyexp.
    Terms of use of the file grants the usual freedoms at the condition
    that no fees are charged upon distribution, making it non-free.
  * Unfuzz patches.
  * d/copyright: declare Open Science Tools Ltd. among copyright owners.
  * d/copyright: Vladimir Kharlampidi is copyright owner of swiper-bundle.
  * d/control: declare compliance to standards version 4.7.0.
  * Rename d/docs to d/psychopy.docs.
  * Rename d/dirs to d/psychopy.dirs.
  * d/rules: ensure the test suite kicks in.
  * d/control: add many new test dependencies.

  [ Andreas Tille ]
  * Do not parse d/changelog (routine-update)
  * Remove constraints unnecessary since bullseye (oldstable):
    + Remove 1 maintscript entries from 1 files.

 TODO: multiple missing dependencies as documented in #953053 are needed
 if one wants to pass the test suite successfully, at least esprima.

  [ Emmanuel Arias ]
  * d/patches/remove-astunparse.patch: Add patch to remove python3-astunparse
    dependency (Closes: #1086772).
    * d/control: Remove python3-astunparse from Build-Depends and Dependency.

 -- Emmanuel Arias <eamanu@debian.org>  Sat, 14 Dec 2024 19:27:16 -0300

psychopy (2023.2.4+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Re-upload as source-only to allow migration

 -- Alexandre Detiste <tchet@debian.org>  Sun, 11 Feb 2024 13:07:18 +0100

psychopy (2023.2.4+dfsg-2) unstable; urgency=medium

  * Team upload.
  * patch-out python3-mock build dependency
  * add recommends on python3-arabic-reshaper (#953053)
  * add patch to avoid generating python3-future dependency

 -- Alexandre Detiste <tchet@debian.org>  Mon, 29 Jan 2024 00:51:58 +0100

psychopy (2023.2.4+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Étienne Mollier ]
  * d/t/run-unit-test: add regression test against #1010537

  [ Andreas Tille ]
  * Fix symlink to DejaVuSerif
    Closes: #1040589
  * Standards-Version: 4.6.2 (routine-update)
  * Do not parse d/changelog (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Build-Depends: pybuild-plugin-pyproject
  * DEP3
  * Build-Depends: python3-pdm-backend
  * Lintian-overrides (see lintian bug #1017966)
  * Recommends: python3-bidi, python3-pyglfw (partly addressing #953053)

 -- Andreas Tille <tille@debian.org>  Wed, 20 Dec 2023 20:10:58 +0100

psychopy (2022.1.3+dfsg-1) unstable; urgency=medium

  * New upstream version
  * d/rules: deduplicate default.mk inclusion.
  * d/control: mark test dependencies as <!nocheck>
  * d/control: depend on DejaVu and OpenSans fonts.
  * d/psychopy.links: use Debian provided fonts instead of embedded ones.
  * d/psychopy.desktop: add Comment= field.
  * d/rules: minor fix to the psychopy(1) manual page title.
  * d/copyright: review and adjustments.

 -- Étienne Mollier <emollier@debian.org>  Sun, 01 May 2022 19:02:14 +0200

psychopy (2022.1.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Depends: python3-astunparse
  * Fix permissions of images properly

 -- Andreas Tille <tille@debian.org>  Sat, 19 Feb 2022 21:20:43 +0100

psychopy (2021.2.3+dfsg-1) unstable; urgency=medium

  * d/watch: fix broken link to github
  * d/control: add myself to uploaders
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * d/control: remove references to unknown packages python3-iolab and
    libavbin0 in Suggests and Conflict fields; they are not part of any
    maintained distribution anymore apparently.
  * Do not parse d/changelog (routine-update)
  * No tab in license text (routine-update)

 -- Étienne Mollier <emollier@debian.org>  Thu, 02 Sep 2021 22:15:03 +0200

psychopy (2020.2.10+dfsg-2) unstable; urgency=medium

  * Team upload
  * d/psychopy.maintscript: gracefully handle transition from symlink to
    regular directory of /usr/share/doc/psychopy/examples from Stretch through
    Buster to Bullseye.
    Closes: #988147

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Fri, 07 May 2021 20:57:53 +0200

psychopy (2020.2.10+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove outdated debian/blends
  * debian/copyright: use spaces rather than tabs to start continuation lines.

 -- Andreas Tille <tille@debian.org>  Fri, 29 Jan 2021 12:15:29 +0100

psychopy (2020.2.1+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * pyxid was accepted to unstable

  [ Nilesh Patra ]
  * Add autopkgtests
  * Install relevant examples
  * Link to demos directory inside examples
  * Repack more files
  * New upstream version 2020.2.1+dfsg
  * Remove unused override

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 08 Aug 2020 20:44:45 +0530

psychopy (2020.1.3+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Do not ship /usr/lib/python3/dist-packages/packaging
    Closes: #953846
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 14 Mar 2020 09:25:36 +0100

psychopy (2020.1.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Depends: s/ipython/ipython3/
    Closes: #937330
  * Do not parse d/changelog (routine-update)
  * Add salsa-ci file (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 06 Mar 2020 16:00:45 +0100

psychopy (2020.1.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Moved package to Debian Med team
  * New upstream version
    Closes: #909559, #878930
  * debhelper-compat 12
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.4.1
  * Do not manually unpack upstream source but use Files-Excluded instead
  * Remove old debian menu file and old style debian/py* files
  * Switch to Python3
    Closes: #937330, #912497, #866455
  * Build-Depends / Recommends: python3-questplus, python3-distro
  * Standards-Version: 4.5.0 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Do not parse d/changelog (routine-update)
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Remove patch wxpython3.0.patch that is missing from
    debian/patches/series.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove deprecated Encoding key from desktop file
    debian/psychopy.desktop.
  * Fix link to examples
  * Drop automatically generated sphinx files from upstream source

 -- Andreas Tille <tille@debian.org>  Mon, 24 Feb 2020 08:56:57 +0100

psychopy (1.90.2.dfsg-1) unstable; urgency=medium

  * Fresh upstream release (Closes: #878930)
  * debian/control
    - replace python-imaging with python-pil (Closes: #866455)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 23 May 2018 09:19:15 -0400

psychopy (1.90.1.dfsg-1) UNRELEASED; urgency=medium

  * Fresh upstream release
  * debian/rules
    - export http*_proxy dummy variables to avoid network

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 06 Apr 2018 00:24:58 -0400

psychopy (1.85.4.dfsg-1) UNRELEASED; urgency=medium

  * Fresh upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 13 Oct 2017 21:16:25 -0400

psychopy (1.85.3.dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 13 Oct 2017 00:00:02 -0400

psychopy (1.85.2.dfsg-1) unstable; urgency=medium

  * New upstream release
    - for now disabling JS (psychojs) support
  * debian/rules
    - run tests using py.test directly
  * debian/rules
    - python-requests to b-depends and recommends
    - wrap running help2man under xvfb as well
  * debian/control
    - added python-future to *Depends since pyglet needs it (I guess misses
      some Depends as well)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 12 Oct 2017 22:32:19 -0400

psychopy (1.85.00.dfsg-1) UNRELEASED; urgency=medium

  * use .png for the psychopy icon within .desktop file
  * debian/patches
    - dropped all changesets picked up from upstream
    - dropped wx patch -- by now upstream is probably supporting it
      better than we would with that patch

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 08 Mar 2017 19:00:05 -0500

psychopy (1.83.04.dfsg-2) unstable; urgency=medium

  * CPed changeset_46fe7ee7a8526a927819e961fbb4404110a87293.diff to overcome
    syntax error during installation.  Thanks Christopher J Markiewicz for the
    report!

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 19 Feb 2016 13:06:09 -0500

psychopy (1.83.04.dfsg-1) unstable; urgency=medium

  * Fresh upstream bugfix (on top of minor not packaged) release
  * debian/patches -- refreshed
  * debian/psychopy.desktop - full path to pixmap (Closes: #693477)
    Thanks Kevin Mitchell
  * debian/watch - adjusted to use github location
  * debian/control
    - python-opencv to Build-Depends and Recommends

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 12 Feb 2016 08:39:20 -0500

psychopy (1.82.02.dfsg-1) unstable; urgency=medium

  * Fresh upstream bugfix release
  * debian/rules
    - make tests less verbose (removed -v, added -q)

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 14 Sep 2015 15:41:28 -0400

psychopy (1.82.01.dfsg-2) unstable; urgency=medium

  * debian/control
    - added configobj explicitly to depends, and xlib to recommends
      (used by iohub)
  * debian/patches/up_skip_wizard_hangingwx
    - to skip test on WX < 3.0 since leads to hanging

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 29 May 2015 12:13:49 -0400

psychopy (1.82.01.dfsg-1) unstable; urgency=medium

  * Fresh upstream bugfix release
  * debian/control
    - python-pandas was added to both Build-depends and Recommends

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 20 Apr 2015 16:23:01 -0400

psychopy (1.81.03.dfsg-1) unstable; urgency=medium

  * New upstream bugfix release
  * debian/rules
    - fix up cleaning (apparently overloading clean:: broke dh_clean)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 03 Dec 2014 11:05:04 -0500

psychopy (1.81.01.dfsg-1) unstable; urgency=medium

  * debian/control
    - Added X-Python-Version: >= 2.7 to avoid problems on older systems
      with unsupported in 2.6 syntax. Also export DEBPYTHON_SUPPORTED in
      debian/rules to solidify such a choice ;-) (Thanks to Mark Hymers)
    - Added Conflicts: libavbin0 (= 7-4+b1) to guarantee that psychopy
      is not installed alongside a broken build of libavbin0.  Presence
      of such a build would impair normal functioning of psychopy.
  * debian/rules
    - fix symlink to demos (now that dh_python doesn't install into
      pyshared)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 23 Oct 2014 14:01:44 -0400

psychopy (1.81.00.dfsg-1) unstable; urgency=medium

  * Fresh upstream release
  * debian/patches
    - update_matplotlib_test -- handled upstream
    - wxpython3.0.patch was refactored into wxpython3.0-1.81.00.patch to
      keep residual changes (suppressing debug output, and use FD_ prefixes)
  * debian/control
    - boost policy to 3.9.6
    - remove libavbin0 from build-depends (broken see #750577)
    - adding python-psutil, python-gevent, python-msgpack, python-yaml,
      python-serial into Build-Depends and Recommends
  * debian/rules
    - state that TRAVIS=true while testing under xvfb to skip some tests
      requiring real hardware etc
    - switch away from pysupport to dh_python
    - switch from cdbs to dh, raised compat to 7
    - for now ignore tests failure(s), they do not exit cleanly
      see https://github.com/psychopy/psychopy/issues/708

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 13 Oct 2014 22:14:30 -0400

psychopy (1.79.00+git16-g30c9343.dfsg-2) UNRELEASED; urgency=medium

  [ Olly Betts ]
  * Update for wxPython 3.0 (Closes: #759090):

  [ Yaroslav Halchenko ]
  * export HOME to be build directory to avoids tests failure
    (Closes: #718154)
  * Allow to build/work against wx 2.8 as well for easier backporting

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 12 Sep 2014 14:35:08 -0400

psychopy (1.79.00+git16-g30c9343.dfsg-1) unstable; urgency=low

  * New release (+ post-release fixes)
  * debian/patches
    - deb_no_post_inst -- upstream does it correctly now
    - update_matplotlib_test (from Mike Hymers) to address missing nxutils
  * debian/control
    - add python-pytest to build-depends (thanks Mark again)
  * debian/rules
    - do not install our runner shell script, just use upstream's runner script
    (renamed)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 21 Dec 2013 10:54:21 -0500

psychopy (1.77.02.dfsg-1) unstable; urgency=low

  * New upstream bugfix release
  * Do not ignore tests failures while building for unstable/sid

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 12 Jul 2013 12:55:11 -0400

psychopy (1.77.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * Enabling build-time unit-testing under Xvfb, but ignoring failures
    for now (there are few differently persistent ones)
  * debian/control:
    - libxxf86vm1 to Recommends -- needed for XF86VidModeSetGammaRamp

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 14 Jun 2013 08:37:23 -0400

psychopy (1.76.00.dfsg-1) experimental; urgency=low

  * New upstream release
  * debian/control:
    - added python-pyo to recommends
  * debian/copyright:
    - updated years
  * debian/patches
    - up* and changeset* removed (since included upstream)
    - deb_no_post_inst updated to not install psychopyApp as a script:
      we have a lean script to invoke it via runpy, and we call it just
      psychopy

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 18 Feb 2013 12:23:31 -0500

psychopy (1.74.03.dfsg-1) experimental; urgency=low

  * New upstream bugfix release

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 14 Aug 2012 13:05:32 -0400

psychopy (1.74.02.dfsg-1) UNRELEASED; urgency=low

  * New bugfix of a fresh upstream feature release.
  * New patches:
    - up_Linux_components_path - point to location on Linux systems, not OSX

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 06 Aug 2012 10:39:21 -0400

psychopy (1.73.06.dfsg-1) unstable; urgency=low

  * New upstream bugfix release (released April 2012)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 27 Jun 2012 15:49:15 -0400

psychopy (1.73.05.dfsg-1) unstable; urgency=low

  * New upstream release
  * Upgraded policy to 3.9.3
    - debian/copyright updated for DEP-5 compliance
  * Removed not any longer needed versioning from build-depends

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 04 Mar 2012 16:19:21 -0500

psychopy (1.73.03+git2-g04717ae.dfsg-1) unstable; urgency=low

  * New upstream release
  * Added python-configobj into build-depends

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 31 Jan 2012 13:26:29 -0500

psychopy (1.71.01.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 31 Oct 2011 21:48:39 -0400

psychopy (1.71.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * Moved scipy into Depends from Recommends due to imports of scipy at
    core modules (also reformatted *Depends into multilines)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 22 Sep 2011 18:31:26 -0400

psychopy (1.70.01.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 07 Sep 2011 15:33:04 -0400

psychopy (1.70.00.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 15 Aug 2011 16:33:41 -0400

psychopy (1.70.00~rc4+git10-gf081945.dfsg-1) unstable; urgency=low

  * New upstream release candidate snapshot

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 08 Aug 2011 17:11:45 -0400

psychopy (1.65.02.dfsg-1) unstable; urgency=low

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 13 Jul 2011 10:37:23 -0400

psychopy (1.65.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * Upgraded policy to 3.9.2 -- no changes

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 03 Jul 2011 20:58:55 -0400

psychopy (1.65.00~pre1.dfsg-1) unstable; urgency=low

  * Getting ready for the upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 22 Jun 2011 20:54:07 -0400

psychopy (1.64.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - added perspective python-pyxid to Suggests
  * debian/copyright:
    - added Jeremey Grey copyright to mseq* code
    - updated years

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 21 Apr 2011 11:08:58 -0400

psychopy (1.63.04.dfsg-1) unstable; urgency=low

  * New upstream bugfix release
  * debian/README.Debian-source: no SVN repository any longer etc.
  * debian/control:
    - updated description
    - removed pyepl from suggests
    - added ipython into Recommends to be used as the Python shell in Coder

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 25 Feb 2011 12:48:15 -0500

psychopy (1.63.01.dfsg-1) experimental; urgency=low

  * New upstream bugfix release

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 14 Jan 2011 12:32:41 -0500

psychopy (1.63.00+git8-g46ee897.dfsg-1) experimental; urgency=low

  * New upstream release:
    - Few post-release commits were included since they fixed some bugs
      and resolved EOLs and executable permissions issues in the repository
  * New patch: deb_do_not_regenerate_init  to leave __init__.py pristine
    even though we ship createInitFile.py

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 20 Dec 2010 12:54:22 -0500

psychopy (1.62.02.dfsg-1) experimental; urgency=low

  * New upstream release
  * Converted into quilt (3.0) sources format -- two patches
    to upstream for DFSG-ed sources are created
  * Elaborated dfsg rule of debian/rules

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 13 Oct 2010 14:08:24 -0400

psychopy (1.62.01.dfsg-1) experimental; urgency=low

  * New upstream bugfix release:
    - removed third-part ioLabs interface (Debian packaging pending)
    - upstreamed moved from SVN to GIT, alioth's repository
      recrerated to follow freshly (properly) converted upstream GIT repo:
      + master-dfsg -> dfsg -- DFSG compliant sources
      + debian-release -> debian -- branch for dfsg+debian/ packaging
  * Added perspective python-iolabs into Suggests

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 20 Sep 2010 11:47:41 -0400

psychopy (1.62.00.dfsg-1) experimental; urgency=low

  * New upstream release. Introduces soft dependency (Recommends)
    on openpyxl
  * Upgraded policy to 3.9.1 -- no changed
  * Upload to experimental due to squeeze freeze

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 18 Aug 2010 12:05:41 -0400

psychopy (1.61.03.dfsg-1) unstable; urgency=low

  * Upstream bugfix release for 1.61 series + 2 additional commits
    (advised by upstream):
    - rev 1015 - builder: minor bug in trying dlgs trying to access advParams
      that don't exist
    - rev 1018 - coder: fix to reading of unicode scripts
  * This should be the last release in 1.61.xx series, 1.62.0 is in alpha
    testing

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 11 Aug 2010 15:33:36 -0400

psychopy (1.61.02.dfsg-2) unstable; urgency=low

  * Debian packaging:
    - Moving under NeuroDebian Team umbrella
    - Boosting policy to 3.9.0 -- no changes seems to be due
    - Adding both python-pyglet and python-pygames to Recommends (they are
      also present in Depends but with the choice of one over the other).
      That should make default installations more coherent and less
      troublesome
  * Absorbed some upstream commits from svn/trunk:
    - lib: fixed bug in filters.makeGrating where gratType='sqr'
    - lib: fixed bug in new color spaces for non-shader code
    - BF: making RaingScale an explicit old-style class
      (otherwise import fails on Python 2.4)
    - removing testmovie.mpg (unknown license), using new and shiny
      testMovie.mp4

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 14 Jul 2010 09:59:29 -0400

psychopy (1.61.02.dfsg-1) unstable; urgency=low

  * New upstream release (+ few svn commits getting to rev1004)
    - no string exceptions (Closes: #585272)
    - includes iolab's Python interface

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 21 Jun 2010 12:05:28 -0400

psychopy (1.61.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * Cherry-picked 2 commits pushed upstream: permissions, and python 2.4
    compatibility

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 12 May 2010 12:55:19 -0400

psychopy (1.60.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control: Upgraded policy to 3.8.4 -- no changes are due
  * debian/copyright: years
  * Include changlog modifications done by upstream between
    announced release and tagging
  * BF: use system-wide installed configobj (Closes: #555341)

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 02 Feb 2010 22:49:11 -0500

psychopy (1.51.00.dfsg-1) unstable; urgency=low

  * Upstream release: based Debian package on SVN rev 687,
    git sha e829fc97e1a23b342630dd10312c345283c915e5.
  * debian/control:
    * BF: Recommends python-scipy
    * RF: libavbin0 into Recommends from Suggests
    * BF: matplotlib/lxml into Depends
    * BF: Adjusted description (closes: #547255)
    * Upgraded policy to 3.8.3 -- no changes are due
  * Reverted to using wrapper script which invokes psychopy app via runpy

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 03 Nov 2009 08:49:32 -0500

psychopy (1.50.04.dfsg-1) unstable; urgency=low

  * New upstream bugfix release

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 15 Sep 2009 11:23:06 -0400

psychopy (1.50.02.dfsg-1) unstable; urgency=low

  * New upstream bugfix release

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 08 Sep 2009 15:19:14 -0400

psychopy (1.50.00.dfsg-1) unstable; urgency=low

  * New upstream release
  * Removed custom psychopy shell script -- now upstream provides a python
    script to start necessary GUI module
  * Adjusted copyright file -- from 1.50.00 psychopy is distributed under
    GPL version 3
  * Added lintian-overrides to keep LICENSE.txt since it is used from GUI

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 01 Sep 2009 13:13:43 -0400

psychopy (1.00.04.dfsg-1) unstable; urgency=low

  * Initial release (Closes: #532111)

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 19 Jun 2009 10:29:52 -0400

psychopy (0.93.5-1) UNRELEASED; urgency=low

  * Initial packaging

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 23 Apr 2008 08:08:17 +0200
